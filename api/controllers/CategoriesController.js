const { Op } = require('sequelize');
const CategoriesModel = require('../models/Categories');

class CategoriesController {

  index = async (req, res, next) => {
    const params = req.query;
    const limit = params.limit || 100;
    const page = params.page || 1;
    const offset = (page - 1) * limit;
    const sort = params.sort || 'id';
    const order = params.order || 'ASC';
    const where = {};

    if (params.name) {
      where.name = {
        [Op.iLike]: `%${params.name}%`
      };
    }

    const categories = await CategoriesModel.findAll({
      where: where,
      limit: limit,
      offset: offset,
      order: [ [sort, order] ]
    });
    res.json(categories);
  }

  create = async (req, res, next) => {
    try {
      const data = await this._validateData(req.body);
      const categories = await CategoriesModel.create(data);
      res.json(categories);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req, res, next) => {
    const categories = await CategoriesModel.findByPk(req.params.categoriesId);
    res.json(categories);
  }

  update = async (req, res, next) => {
    try {
      const id = req.params.categoriesId;
      const data = await this._validateData(req.body, id);
      await CategoriesModel.update(data, {
        where: {
          id: id
        }
      });
      res.json(await CategoriesModel.findByPk(id));
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req, res, next) => {
    await CategoriesModel.destroy({
      where: {
        id: req.params.categoriesId
      }
    });
    res.json({});
  }

  _validateData = async (data) => {
    const attributes = ['description'];
    const categories = {};
    for (const attribute of attributes) {
      if (! data[attribute]){
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      categories[attribute] = data[attribute];
    }

    return categories;
  }

}

module.exports = new CategoriesController();
