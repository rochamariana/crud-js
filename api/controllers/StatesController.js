const { Op } = require('sequelize');
const StatesModel = require('../models/States');

class StatesController {

  index = async (req, res, next) => {
    const params = req.query;
    const limit = params.limit || 100;
    const page = params.page || 1;
    const offset = (page - 1) * limit;
    const sort = params.sort || 'id';
    const order = params.order || 'ASC';
    const where = {};

    if (params.name) {
      where.name = {
        [Op.iLike]: `%${params.name}%`
      };
    }

    const state = await StatesModel.findAll({
      where: where,
      limit: limit,
      offset: offset,
      order: [ [sort, order] ]
    });
    res.json(state);
  }

  create = async (req, res, next) => {
    try {
      const data = await this._validateData(req.body);
      const state = await StatesModel.create(data);
      res.json(state);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req, res, next) => {
    const state = await StatesModel.findByPk(req.params.statesId);
    res.json(state);
  }

  update = async (req, res, next) => {
    try {
      const id = req.params.statesId;
      const data = await this._validateData(req.body, id);
      await StatesModel.update(data, {
        where: {
          id: id
        }
      });
      res.json(await StatesModel.findByPk(id));
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req, res, next) => {
    await StatesModel.destroy({
      where: {
        id: req.params.statesId
      }
    });
    res.json({});
  }

  _validateData = async (data) => {
    const attributes = ['name', 'province'];
    const state = {};
    for (const attribute of attributes) {
      if (! data[attribute]){
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      state[attribute] = data[attribute];
    }

    return state;
  }

}

module.exports = new StatesController();
