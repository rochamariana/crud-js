const { Op } = require('sequelize');
const CitiesModel = require('../models/Cities');
const StatesModel = require('../models/States');

class CitiesController {

  index = async (req, res, next) => {
    const params = req.query;
    const limit = params.limit || 100;
    const page = params.page || 1;
    const offset = (page - 1) * limit;
    const sort = params.sort || 'id';
    const order = params.order || 'ASC';
    const where = {};

    if (params.name) {
      where.name = {
        [Op.iLike]: `%${params.name}%`
      };
    }

    const cities = await CitiesModel.findAll({
      include: [{
        model: StatesModel,
        required: false,
        attributes: ['name']
      }]
    });
    res.json(cities);
  }

  create = async (req, res, next) => {
    try {
      const data = await this._validateData(req.body);
      const cities = await CitiesModel.create(data);
      res.json(cities);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req, res, next) => {
    const cities = await CitiesModel.findByPk(req.params.citiesId);
    res.json(cities);
  }

  update = async (req, res, next) => {
    try {
      const id = req.params.citiesId;
      const data = await this._validateData(req.body, id);
      await CitiesModel.update(data, {
        where: {
          id: id
        }
      });
      res.json(await CitiesModel.findByPk(id));
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req, res, next) => {
    await CitiesModel.destroy({
      where: {
        id: req.params.citiesId
      }
    });
    res.json({});
  }

  _validateData = async (data) => {
    const attributes = ['name', 'StateId'];
    const cities = {};
    for (const attribute of attributes) {
      if (!data[attribute]) {
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      cities[attribute] = data[attribute];
    }

    return cities;
  }

}

module.exports = new CitiesController();
