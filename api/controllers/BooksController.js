const { Op } = require('sequelize');
const BooksModel = require('../models/Books');
const PublishersModel = require('../models/Publishers');
const CategoriesModel = require('../models/Categories');

class BooksController {

  index = async (req, res, next) => {
    const params = req.query;
    const limit = params.limit || 100;
    const page = params.page || 1;
    const offset = (page - 1) * limit;
    const sort = params.sort || 'id';
    const order = params.order || 'ASC';
    const where = {};

    if (params.name) {
      where.name = {
        [Op.iLike]: `%${params.name}%`
      };
    }

    const book = await BooksModel.findAll({
      where: where,
      limit: limit,
      offset: offset,
      order: [ [sort, order] ],
      include: [{
        model: PublishersModel,
        required: false,
        attributes: ['name']
      },
      {
        model: CategoriesModel,
        required: false,
        attributes: ['description']
      },]
    });
    res.json(book);
  }

  create = async (req, res, next) => {
    try {
      const data = await this._validateData(req.body);
      const book = await BooksModel.create(data);
      res.json(book);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req, res, next) => {
    const book = await BooksModel.findByPk(req.params.booksId);
    res.json(book);
  }

  update = async (req, res, next) => {
    try {
      const id = req.params.booksId;
      const data = await this._validateData(req.body, id);
      await BooksModel.update(data, {
        where: {
          id: id
        }
      });
      res.json(await BooksModel.findByPk(id));
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req, res, next) => {
    await BooksModel.destroy({
      where: {
        id: req.params.booksId
      }
    });
    res.json({});
  }

  _validateData = async (data, id) => {
    const attributes = ['title', 'author', 'publication_year', 'pages', 'CategoryId', 'PublisherId'];
    const book = {};
    for (const attribute of attributes) {
      if (! data[attribute]){
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      book[attribute] = data[attribute];
    }

    return book;
  }

}

module.exports = new BooksController();
