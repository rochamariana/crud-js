const { Op } = require('sequelize');
const PublishersModel = require('../models/Publishers');
const CitiesModel = require('../models/Cities');

class PublishersController {

  index = async (req, res, next) => {
    const params = req.query;
    const limit = params.limit || 100;
    const page = params.page || 1;
    const offset = (page - 1) * limit;
    const sort = params.sort || 'id';
    const order = params.order || 'ASC';
    const where = {};

    if (params.name) {
      where.name = {
        [Op.iLike]: `%${params.name}%`
      };
    }

    const publisher = await PublishersModel.findAll({
      where: where,
      limit: limit,
      offset: offset,
      order: [ [sort, order] ],
      include: [{
        model: CitiesModel,
        required: false,
        attributes: ['name']
      }]
    });
    res.json(publisher);
  }

  create = async (req, res, next) => {
    try {
      const data = await this._validateData(req.body);
      const publisher = await PublishersModel.create(data);
      res.json(publisher);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req, res, next) => {
    const publisher = await PublishersModel.findByPk(req.params.publishersId);
    res.json(publisher);
  }

  update = async (req, res, next) => {
    try {
      const id = req.params.publishersId;
      const data = await this._validateData(req.body, id);
      await PublishersModel.update(data, {
        where: {
          id: id
        }
      });
      res.json(await PublishersModel.findByPk(id));
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req, res, next) => {
    await PublishersModel.destroy({
      where: {
        id: req.params.publishersId
      }
    });
    res.json({});
  }

  _validateData = async (data, id) => {
    const attributes = ['name', 'CityId'];
    const publisher = {};
    for (const attribute of attributes) {
      if (! data[attribute]){
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      publisher[attribute] = data[attribute];
    }

    return publisher;
  }

}

module.exports = new PublishersController();
