const cors = require('cors');
const router = require('express').Router();

const users = require('./users');
const states = require('./states');
const categories = require('./categories');
const cities = require('./cities');
const publishers = require('./publishers');
const books = require('./books');

router.use(cors());

router.use(users);
router.use(states);
router.use(categories);
router.use(cities);
router.use(publishers);
router.use(books);

module.exports = router;
