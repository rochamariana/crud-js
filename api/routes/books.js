const router = require('express').Router();
const BooksModel = require('../models/Books');
const booksController = require('../controllers/BooksController');

const validateBookId = async (req, res, next) => {
  const books = await BooksModel.findByPk(req.params.booksId);
  if (!books) {
    return res.status(404).json({ error: 'Book not found' });
  }
  next();
}

router.get('/books', booksController.index);

router.post('/books', booksController.create);

router.get('/books/:booksId', validateBookId, booksController.show);

router.put('/books/:booksId', validateBookId, booksController.update);

router.delete('/books/:booksId', validateBookId, booksController.delete);

module.exports = router;