const router = require('express').Router();
const CategoriesModel = require('../models/Categories');
const categoriesController = require('../controllers/CategoriesController');

const validateCategoriesId = async (req, res, next) => {
  const category = await CategoriesModel.findByPk(req.params.categoriesId);
  if (!category) {
    return res.status(404).json({ error: 'Category not found' });
  }
  next();
}
 
router.get('/categories', categoriesController.index);

router.post('/categories', categoriesController.create);

router.get('/categories/:categoriesId', validateCategoriesId, categoriesController.show);

router.put('/categories/:categoriesId', validateCategoriesId, categoriesController.update);

router.delete('/categories/:categoriesId', validateCategoriesId, categoriesController.delete);

module.exports = router;
