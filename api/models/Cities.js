const { DataTypes, Model } = require('sequelize');
const db = require('../db');
const States = require('./States');

class Cities extends Model { };

Cities.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING(45),
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'cities',
  modelName: 'Cities'
});

States.hasMany(Cities);
Cities.belongsTo(States);

module.exports = Cities;