const { DataTypes, Model } = require('sequelize');
const db = require('../db');

class States extends Model { };

States.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  }, 
  name: {
    type: DataTypes.STRING(45),
    allowNull: false
  },
  province: {
    type: DataTypes.STRING(2),
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'states',
  modelName: 'States'
});

module.exports = States;