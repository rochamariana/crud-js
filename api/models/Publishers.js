const { DataTypes, Model } = require('sequelize');
const db = require('../db');
const Cities = require('./Cities');

class Publishers extends Model { };

Publishers.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING(45),
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'publishers',
  modelName: 'Publishers'
});

Cities.hasMany(Publishers);
Publishers.belongsTo(Cities);

module.exports = Publishers;