const { DataTypes, Model } = require('sequelize');
const db = require('../db');
const Publishers = require('./Publishers');
const Categories = require('./Categories');

class Books extends Model { };

Books.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  title: {
    type: DataTypes.STRING(45),
    allowNull: false
  },
  author: {
    type: DataTypes.STRING(45),
    allowNull: false
  },
  publication_year: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  pages: {
    type: DataTypes.INTEGER,
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'books',
  modelName: 'Books'
});

Publishers.hasMany(Books);
Books.belongsTo(Publishers);

Categories.hasMany(Books);
Books.belongsTo(Categories);

module.exports = Books;