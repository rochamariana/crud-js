const ENDPOINT = "http://localhost:3000";

const loadTable = () => {
    axios.get(`${ENDPOINT}/categories`)
        .then((response) => {
            if (response.status === 200) {
                const data = response.data;
                var trHTML = '';
                data.forEach(element => {
                    trHTML += '<tr>';
                    trHTML += '<td>' + element.id + '</td>';
                    trHTML += '<td>' + element.description + '</td>';
                    trHTML += '<td><button type="button" class="btn btn-outline-secondary" onclick="showItemEditBox(' + element.id + ')">Edit</button>';
                    trHTML += '<button type="button" class="btn btn-outline-danger" onclick="itemDelete(' + element.id + ')">Del</button></td>';
                    trHTML += "</tr>";
                });
                document.getElementById("mytable").innerHTML = trHTML;
            }
        })
};

loadTable();

const itemCreate = () => {
    const description = document.getElementById("description").value;

    axios.post(`${ENDPOINT}/categories`, {
        description: description
    })
        .then((response) => {
            Swal.fire(`Categoria ${response.data.description} cadastrada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao cadastrar nova categoria: ${error.response.data.error} `)
                .then(() => {
                    showItemCreateBox();
                })
        });
}

const getItem = (id) => {
    return axios.get(`${ENDPOINT}/categories/` + id);
}

const itemEdit = () => {
    const id = document.getElementById("id").value;
    const description = document.getElementById("description").value;

    axios.put(`${ENDPOINT}/categories/` + id, {
        description: description
    })
        .then((response) => {
            Swal.fire(`Categoria ${response.data.description} atualizada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao atualizar categoria: ${error.response.data.error} `)
                .then(() => {
                    showItemEditBox(id);
                })
        });
}

const itemDelete = async (id) => {
    const categories = await getItem(id);
    const data = categories.data;
    axios.delete(`${ENDPOINT}/categories/` + id)
        .then((response) => {
            Swal.fire(`Categoria ${data.description} deletada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao deletar categoria: ${error.response.data.error} `);
            loadTable();
        });
};

const showItemCreateBox = () => {
    Swal.fire({
        title: 'Cadastrar nova Categoria',
        html:
            '<input id="id" type="hidden">' +
            '<input id="description" class="swal2-input" placeholder="Descrição">',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemCreate();
        }
    });
}

const showItemEditBox = async (id) => {
    const user = await getItem(id);
    const data = user.data;
    Swal.fire({
        title: 'Editar Categoria',
        html:
            '<input id="id" type="hidden" value="' + data.id + '">' +
            '<input id="description" class="swal2-input" placeholder="Nome" value="' + data.description + '">',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemEdit();
        }
    });

}
