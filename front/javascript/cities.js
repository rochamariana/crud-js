const ENDPOINT = "http://localhost:3000";

const loadTable = () => {
    axios.get(`${ENDPOINT}/cities`)
        .then((response) => {
            if (response.status === 200) {
                const data = response.data;
                var trHTML = '';
                data.forEach(element => {
                    trHTML += '<tr>';
                    trHTML += '<td>' + element.id + '</td>';
                    trHTML += '<td>' + element.name + '</td>';
                    trHTML += '<td>' + element.State.name + '</td>';
                    trHTML += '<td><button type="button" class="btn btn-outline-secondary" onclick="showItemEditBox(' + element.id + ')">Edit</button>';
                    trHTML += '<button type="button" class="btn btn-outline-danger" onclick="itemDelete(' + element.id + ')">Del</button></td>';
                    trHTML += "</tr>";
                });
                document.getElementById("mytable").innerHTML = trHTML;
            }
        })
};

loadTable();

const itemCreate = () => {
    const name = document.getElementById("name").value;

    const select = document.getElementById("StateId");
    const StateId = select.options[select.selectedIndex].value;

    axios.post(`${ENDPOINT}/cities`, {
        name: name,
        StateId: StateId
    })
        .then((response) => {
            Swal.fire(`Cidade ${response.data.name} cadastrada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao cadastrar nova cidade: ${error.response.data.error} `)
                .then(() => {
                    showItemCreateBox();
                })
        });
}

const getItem = (id) => {
    return axios.get(`${ENDPOINT}/cities/` + id);
}

const itemEdit = () => {
    const id = document.getElementById("id").value;
    const name = document.getElementById("name").value;

    const select = document.getElementById("StateId");
    const StateId = select.options[select.selectedIndex].value;

    axios.put(`${ENDPOINT}/cities/` + id, {
        name: name,
        StateId: StateId
    })
        .then((response) => {
            Swal.fire(`Cidade ${response.data.name} atualizada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao atualizar cidade: ${error.response.data.error} `)
                .then(() => {
                    showItemEditBox(id);
                })
        });
}

const itemDelete = async (id) => {
    const cities = await getItem(id);
    const data = cities.data;

    axios.delete(`${ENDPOINT}/cities/` + id)
        .then((response) => {
            Swal.fire(`Cidade ${data.name} deletada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao deletar cidade: ${error.response.data.error} `);
            loadTable();
        });
};

const _getStates = async () => {
    const response = await axios.get(`${ENDPOINT}/states/`);
    const data = response.data;
    var select = document.querySelector('select#StateId');
    data.forEach(element => {
        let option = `<option value="${element.id}"> ${element.name} </option>`;
        select.innerHTML += option;
    });
}


const showItemCreateBox = () => {
    _getStates();

    Swal.fire({
        title: 'Cadastrar nova Cidade',
        html:
            '<input id="id" type="hidden">' +
            '<input id="name" class="swal2-input" placeholder="Nome">' +
            '<select id="StateId" class="swal2-input"><option value="" disabled selected>Selecione um estado</option></select>',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemCreate();
        }
    });
}

const showItemEditBox = async (id) => {
    const cities = await getItem(id);
    const data = cities.data;  
    _getStates();  
    Swal.fire({
        title: 'Editar Cidade',
        html:
            '<input id="id" type="hidden" value="' + data.id + '">' +
            '<input id="name" class="swal2-input" placeholder="Nome" value="' + data.name + '">' +
            '<select id="StateId" class="swal2-input"><option value="" disabled selected>Selecione um estado</option></select>',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemEdit();
        }
    });

}

