const ENDPOINT = "http://localhost:3000";

const loadTable = () => {
    axios.get(`${ENDPOINT}/books`)
        .then((response) => {
            if (response.status === 200) {
                const data = response.data;
                var trHTML = '';
                data.forEach(element => {
                    trHTML += '<tr>';
                    trHTML += '<td>' + element.id + '</td>';
                    trHTML += '<td>' + element.title + '</td>';
                    trHTML += '<td>' + element.author + '</td>';
                    trHTML += '<td>' + element.publication_year + '</td>';
                    trHTML += '<td>' + element.pages + '</td>';
                    trHTML += '<td>' + element.Category.description + '</td>';
                    trHTML += '<td>' + element.Publisher.name + '</td>';
                    trHTML += '<td><button type="button" class="btn btn-outline-secondary" onclick="showItemEditBox(' + element.id + ')">Edit</button>';
                    trHTML += '<button type="button" class="btn btn-outline-danger" onclick="itemDelete(' + element.id + ')">Del</button></td>';
                    trHTML += "</tr>";
                });
                document.getElementById("mytable").innerHTML = trHTML;
            }
        })
};

loadTable();

const itemCreate = () => {
    const title = document.getElementById("title").value;
    const author = document.getElementById("author").value;
    const publication_year = document.getElementById("publication_year").value;
    const pages = document.getElementById("pages").value;
        
    const select = document.getElementById("CategoryId");
    const CategoryId = select.options[select.selectedIndex].value;

    const select1 = document.getElementById("PublisherId");
    const PublisherId = select1.options[select1.selectedIndex].value;

    axios.post(`${ENDPOINT}/books`, {
        title: title,
        author: author,
        publication_year: publication_year,
        pages: pages,
        CategoryId: CategoryId,
        PublisherId: PublisherId

    })
        .then((response) => {
            Swal.fire(`Livro ${response.data.title} cadastrado`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao cadastrar novo livro: ${error.response.data.error} `)
                .then(() => {
                    showItemCreateBox();
                })
        });
}

const getItem = (id) => {
    return axios.get(`${ENDPOINT}/books/` + id);
}

const itemEdit = () => {
    const id = document.getElementById("id").value;
    const title = document.getElementById("title").value;
    const author = document.getElementById("author").value;
    const publication_year = document.getElementById("publication_year").value;
    const pages = document.getElementById("pages").value;

    const select = document.getElementById("CategoryId");
    const CategoryId = select.options[select.selectedIndex].value;

    const select1 = document.getElementById("PublisherId");
    const PublisherId = select1.options[select1.selectedIndex].value;

    axios.put(`${ENDPOINT}/books/` + id, {
        title: title,
        author: author,
        publication_year: publication_year,
        pages: pages,
        CategoryId: CategoryId,
        PublisherId: PublisherId
    })
        .then((response) => {
            Swal.fire(`Livro ${response.data.title} atualizado`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao atualizar livro: ${error.response.data.error} `)
                .then(() => {
                    showItemEditBox(id);
                })
        });
}

const itemDelete = async (id) => {
    const book = await getItem(id);
    const data = book.data;
    axios.delete(`${ENDPOINT}/books/` + id)
        .then((response) => {
            Swal.fire(`Livro ${data.title} deletado`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao deletar livro: ${error.response.data.error} `);
            loadTable();
        });
};

const _getPublishers = async () => {
    const response = await axios.get(`${ENDPOINT}/publishers/`);
    const data = response.data;
    var select = document.querySelector('select#PublisherId');
    data.forEach(element => {
        let option = `<option value="${element.id}"> ${element.name} </option>`;
        select.innerHTML += option;
    });
}

const _getCategories = async () => {
    const response = await axios.get(`${ENDPOINT}/categories/`);
    const data = response.data;
    var select = document.querySelector('select#CategoryId');
    data.forEach(element => {
        let option = `<option value="${element.id}"> ${element.description} </option>`;
        select.innerHTML += option;
    });
}


const showItemCreateBox = () => {
    _getCategories();
    _getPublishers();

    Swal.fire({
        title: 'Cadastrar novo Livro',
        html:
            '<input id="id" type="hidden">' +
            '<input id="title" class="swal2-input" placeholder="Título">' +
            '<input id="author" class="swal2-input" placeholder="Autor">' +
            '<input id="publication_year" class="swal2-input" placeholder="Ano de Publicação">' +
            '<input id="pages" class="swal2-input" placeholder="Páginas">' +
            '<select id="CategoryId" class="swal2-input"><option value="" disabled selected>Selecione uma categoria</option></select>' +
            '<select id="PublisherId" class="swal2-input"><option value="" disabled selected>Selecione uma editora</option></select>',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemCreate();
        }
    });
}

const showItemEditBox = async (id) => {
    const book = await getItem(id);
    const data = book.data;

    _getCategories();
    _getPublishers();

    Swal.fire({
        title: 'Editar Livro',
        html:
            '<input id="id" type="hidden" value=' + data.id + '>' +
            '<input id="title" class="swal2-input" placeholder="Título" value="' + data.title + '">' +
            '<input id="author" class="swal2-input" placeholder="Autor" value="' + data.author + '">' +
            '<input id="publication_year" class="swal2-input" placeholder="Ano de Publicação" value="' + data.publication_year + '">' +
            '<input id="pages" class="swal2-input" placeholder="Página" value="' + data.pages + '">' +
            '<select id="CategoryId" class="swal2-input"><option value="" disabled selected>Selecione uma categoria</option></select>' +
            '<select id="PublisherId" class="swal2-input"><option value="" disabled selected>Selecione uma editora</option></select>',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemEdit();
        }
    });
}
