const ENDPOINT = "http://localhost:3000";

const loadTable = () => {
    axios.get(`${ENDPOINT}/publishers`)
        .then((response) => {
            if (response.status === 200) {
                const data = response.data;
                var trHTML = '';
                data.forEach(element => {
                    trHTML += '<tr>';
                    trHTML += '<td>' + element.id + '</td>';
                    trHTML += '<td>' + element.name + '</td>';
                    trHTML += '<td>' + element.City.name + '</td>';
                    trHTML += '<td><button type="button" class="btn btn-outline-secondary" onclick="showItemEditBox(' + element.id + ')">Edit</button>';
                    trHTML += '<button type="button" class="btn btn-outline-danger" onclick="itemDelete(' + element.id + ')">Del</button></td>';
                    trHTML += "</tr>";
                });
                document.getElementById("mytable").innerHTML = trHTML;
            }
        })
};

loadTable();

const itemCreate = () => {
    const name = document.getElementById("name").value;
    const select = document.getElementById("CityId");
    const CityId = select.options[select.selectedIndex].value;

    axios.post(`${ENDPOINT}/publishers`, {
        name: name,
        CityId: CityId
    })
        .then((response) => {
            Swal.fire(`Editora ${response.data.name} cadastrada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao cadastrar nova editora: ${error.response.data.error} `)
                .then(() => {
                    showItemCreateBox();
                })
        });
}

const getItem = (id) => {
    return axios.get(`${ENDPOINT}/publishers/` + id);
}

const itemEdit = () => {
    const id = document.getElementById("id").value;
    const name = document.getElementById("name").value;

    const select = document.getElementById("CityId");
    const CityId = select.options[select.selectedIndex].value;

    axios.put(`${ENDPOINT}/publishers/` + id, {
        name: name,
        CityId: CityId
    })
        .then((response) => {
            Swal.fire(`Editora ${response.data.name} atualizada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao atualizar editora: ${error.response.data.error} `)
                .then(() => {
                    showItemEditBox(id);
                })
        });
}

const itemDelete = async (id) => {
    const publishers = await getItem(id);
    const data = publishers.data;

    axios.delete(`${ENDPOINT}/publishers/` + id)
        .then((response) => {
            Swal.fire(`Editora ${data.name} deletada`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao deletar editora: ${error.response.data.error} `);
            loadTable();
        });
};

const _getCities = async () => {
    const response = await axios.get(`${ENDPOINT}/cities/`);
    const data = response.data;
    var select = document.querySelector('select#CityId');
    data.forEach(element => {
        let option = `<option value="${element.id}"> ${element.name} </option>`;
        select.innerHTML += option;
    });
}

const showItemCreateBox = () => {
    _getCities();
    Swal.fire({
        title: 'Cadastrar nova Editora',
        html:
            '<input id="id" type="hidden">' +
            '<input id="name" class="swal2-input" placeholder="Nome">' +
            '<select id="CityId" class="swal2-input"><option value="" disabled selected>Selecione uma cidade</option></select>',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemCreate();
        }
    });
}

const showItemEditBox = async (id) => {
    const publishers = await getItem(id);
    const data = publishers.data;
    _getCities();
    Swal.fire({
        title: 'Editar Editora',
        html:
            '<input id="id" type="hidden" value="' + data.id + '">' +
            '<input id="name" class="swal2-input" placeholder="Nome" value="' + data.name + '">' +
            '<select id="CityId" class="swal2-input"><option value="" disabled selected>Selecione uma cidade</option></select>',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemEdit();
        }
    });

}