const ENDPOINT = "http://localhost:3000";

const loadTable = () => {
    axios.get(`${ENDPOINT}/states`)
        .then((response) => {
            if (response.status === 200) {
                const data = response.data;
                var trHTML = '';
                data.forEach(element => {
                    trHTML += '<tr>';
                    trHTML += '<td>' + element.id + '</td>';
                    trHTML += '<td>' + element.name + '</td>';
                    trHTML += '<td>' + element.province + '</td>';
                    trHTML += '<td><button type="button" class="btn btn-outline-secondary" onclick="showItemEditBox(' + element.id + ')">Edit</button>';
                    trHTML += '<button type="button" class="btn btn-outline-danger" onclick="itemDelete(' + element.id + ')">Del</button></td>';
                    trHTML += "</tr>";
                });
                document.getElementById("mytable").innerHTML = trHTML;
            }
        })
};

loadTable();

const itemCreate = () => {
    const name = document.getElementById("name").value;
    const province = document.getElementById("province").value;

    axios.post(`${ENDPOINT}/states`, {
        name: name,
        province: province
    })
        .then((response) => {
            Swal.fire(`Estado ${response.data.name} cadastrado`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao cadastrar novo estado: ${error.response.data.error} `)
                .then(() => {
                    showItemCreateBox();
                })
        });
}

const getItem = (id) => {
    return axios.get(`${ENDPOINT}/states/` + id);
}

const itemEdit = () => {
    const id = document.getElementById("id").value;
    const name = document.getElementById("name").value;
    const province = document.getElementById("province").value;

    axios.put(`${ENDPOINT}/states/` + id, {
        name: name,
        province: province
    })
        .then((response) => {
            Swal.fire(`Estado ${response.data.name} atualizado`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao atualizar estado: ${error.response.data.error} `)
                .then(() => {
                    showItemEditBox(id);
                })
        });
}

const itemDelete = async (id) => {
    const states = await getItem(id);
    const data = states.data;

    axios.delete(`${ENDPOINT}/states/` + id)
        .then((response) => {
            Swal.fire(`Estado ${data.name} deletado`);
            loadTable();
        }, (error) => {
            Swal.fire(`Erro ao deletar estado: ${error.response.data.error} `);
            loadTable();
        });
};

const showItemCreateBox = () => {
    Swal.fire({
        title: 'Cadastrar novo Estado',
        html:
            '<input id="id" type="hidden">' +
            '<input id="name" class="swal2-input" placeholder="Nome">' +
            '<input id="province" class="swal2-input" placeholder="UF">',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemCreate();
        }
    });
}

const showItemEditBox = async (id) => {
    const user = await getItem(id);
    const data = user.data;
    Swal.fire({
        title: 'Editar Estado',
        html:
            '<input id="id" type="hidden" value="' + data.id + '">' +
            '<input id="name" class="swal2-input" placeholder="Nome" value="' + data.name + '">' +
            '<input id="province" class="swal2-input" placeholder="UF" value="' + data.province + '">',
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
            itemEdit();
        }
    });

}